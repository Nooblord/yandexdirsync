# Import external module
import ldap


class LDAPDirectory(object):
    """LDAP Directory class"""
    def __init__(self, url, user, password, basedn, dirtype,
                 excludedisabled):
        self.url = url
        self.user = user
        self.password = password
        self.basedn = basedn
        self.type = dirtype
        self.excludedisabled = excludedisabled
        self.connect()

    @staticmethod
    def parseldap(value, indexstring):
        try:
            results = eval('value' + indexstring)
            if type(results) is list:
                return list(map(lambda x: x.decode('utf-8'), results))
            else:
                return results.decode("utf-8")
        except Exception:
            return ""

    def connect(self):
        ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)
        self.ldapconnection = ldap.initialize(self.url)
        self.ldapconnection.protocol_version = ldap.VERSION3
        print("Connected")

    def authorize(self):
        self.ldapconnection.simple_bind_s(self.user, self.password)
        print("Authorized")

    def process_ldap_result(self, ldap_result):
        result_set = []
        while 1:
            result_type, result_data = self.ldapconnection.result(ldap_result,
                                                                  0)
            if (result_data == []):
                break
            else:
                if result_type == ldap.RES_SEARCH_ENTRY:
                    result_set.append(result_data)
        return result_set

    def get_users(self):
        excludeattr = ""
        self.users = {}
        self.groups = {}
        if (self.type == 'AD'):
            if (self.excludedisabled):
                excludeattr = "(!userAccountControl:1.2.840.113556.1.4.803:=2)"
            ldap_result_user = self.ldapconnection.search(
                self.basedn,
                ldap.SCOPE_SUBTREE,
                "(&(objectCategory=person)\
                (objectClass=user)\
                (mail=*)" + excludeattr + ")")
            ldap_result_group = self.ldapconnection.search(
                self.basedn,
                ldap.SCOPE_SUBTREE,
                "(&(objectClass=group))")
        elif (self.type == 'OD'):
            ldap_result_user = self.ldapconnection.search(
                self.basedn,
                ldap.SCOPE_SUBTREE,
                "(&(objectClass=inetOrgPerson)\
                (mail=*))")
            ldap_result_group = self.ldapconnection.search(
                self.basedn,
                ldap.SCOPE_SUBTREE,
                "(&(objectClass=posixGroup)\
                (memberUid=*)(mail=*))")
            ldap_disabled_group = self.ldapconnection.search(
                self.basedn,
                ldap.SCOPE_SUBTREE,
                "(&(objectClass=posixGroup)\
                (cn=com.apple.access_disabled))")
        elif (self.type == 'OL'):
            ldap_result_user = self.ldapconnection.search(
                self.basedn,
                ldap.SCOPE_SUBTREE,
                "(&(objectClass=inetOrgPerson)\
                (mail=*))")
            ldap_result_group = self.ldapconnection.search(
                self.basedn,
                ldap.SCOPE_SUBTREE,
                "(&(objectClass=posixGroup)\
                (mail=*))")

        self.users_result = self.process_ldap_result(ldap_result_user)
        self.groups_result = self.process_ldap_result(ldap_result_group)

        if (self.type == 'OD'):
            self.disabled_result = self.process_ldap_result(
                ldap_disabled_group)

            for user in self.users_result:
                self.users[
                    self.parseldap(user, "[0][1]['uid'][0]")] = {
                    "firstname": self.parseldap(user,
                                                "[0][1]['givenName'][0]"),
                    "lastname": self.parseldap(user, "[0][1]['sn'][0]"),
                    "fullname": self.parseldap(user, "[0][1]['cn'][0]"),
                    "username": self.parseldap(user, "[0][1]['uid'][0]"),
                    "email": self.parseldap(user, "[0][1]['mail'][0]"),
                    "groups": []}

            for group in self.groups_result:
                if (self.parseldap(group, "[0][1]['cn'][0]") != ""):
                    self.groups[
                        self.parseldap(group, "[0][1]['cn'][0]")] = {
                        "fullname": self.parseldap(group, "[0][1]\
                            ['apple-group-realname'][0]"),
                        "name": self.parseldap(group, "[0][1]['cn'][0]"),
                        "mail": self.parseldap(group, "[0][1]['mail'][0]"),
                        "members": self.parseldap(group,
                                                  "[0][1]['memberUid']")}
            # Search for disabled users in OD
            for disabled in self.disabled_result:
                self.disabled_users = self.parseldap(
                    disabled, "[0][1]['memberUid']")

            # Exclude disabled users in OD
            if self.excludedisabled:
                for user in self.disabled_users:
                    del self.users[user]

            # Add groups to users
            for group in self.groups:
                for member in self.groups[group]['members']:
                    # Remove disabled members from group
                    if member in self.disabled_users:
                        self.groups[group]['members'].remove(member)
                    if member in dict.keys(self.users):
                        self.users[member]['groups'].append(group)

        if (self.type == 'AD'):
            for user in self.users_result:
                self.users[
                    self.parseldap(user, "[0][1]['uid'][0]")] = {
                    "firstname": self.parseldap(user,
                                                "[0][1]['givenName'][0]"),
                    "lastname": self.parseldap(user, "[0][1]['sn'][0]"),
                    "fullname": self.parseldap(user, "[0][1]['cn'][0]"),
                    "username": self.parseldap(user, "[0][1]['uid'][0]"),
                    "email": self.parseldap(user, "[0][1]['mail'][0]"),
                    "groups": []}

            for group in self.groups_result:
                if (self.parseldap(group, "[0][1]['cn'][0]") != ""):
                    self.groups[
                        self.parseldap(group, "[0][1]['cn'][0]")] = {
                        "fullname": self.parseldap(group, "[0][1]\
                            ['apple-group-realname'][0]"),
                        "name": self.parseldap(group, "[0][1]['cn'][0]"),
                        "mail": self.parseldap(group, "[0][1]['mail'][0]"),
                        "members": self.parseldap(group,
                                                  "[0][1]['memberUid']")}

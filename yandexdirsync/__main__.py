"""Sync your AD/LDAP/Whaterer with Yandex.Connect Directory """

# Import internal modules
import os
import argparse
import configparser
from pprint import pprint

# Import classes
from .LDAPDirectory import LDAPDirectory

# Define globals
CONFIG_FILE = 'yandexdirsync.conf'
CONFIG_LOCATIONS = ['/etc',
                    '/usr/local/etc',
                    os.curdir]


def main():
    global CONFIG_LOCATIONS, CONFIG_FILE
    # Parse arguments
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('-c', '--config', help="specify config file")
    args = vars(parser.parse_args())

    cp = configparser.SafeConfigParser()
    if args["config"]:
        config_file = cp.read(args["config"])
    else:
        config_file = cp.read([os.path.join(confpath, CONFIG_FILE) for
                               confpath in CONFIG_LOCATIONS])
    if cp is not None:
        ldap_url = cp.get("LDAP", "url")
        ldap_basedn = cp.get("LDAP", "basedn")
        ldap_login = cp.get("LDAP", "user")
        ldap_password = cp.get("LDAP", "password")
        ldap_type = cp.get("LDAP", "type", fallback="OD")
        ldap_excludedisabled = cp.get(
            "LDAP", "excludedisabled", fallback=True)
    else:
        print("Please provide config")
        return 1

    LDAPDir = LDAPDirectory(ldap_url, ldap_login,
                            ldap_password, ldap_basedn,
                            ldap_type, ldap_excludedisabled)
    LDAPDir.authorize()
    print(config_file)
    LDAPDir.get_users()
    pprint(LDAPDir.users_result)
    pprint(LDAPDir.groups_result)
    print("main() end")


if __name__ == "__main__":
    main()

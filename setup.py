#!/usr/bin/env python3

from setuptools import setup, find_packages
from os.path import join, dirname
import yandexdirsync


with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
    name='yandexdirsync',
    version=yandexdirsync.__version__,
    packages=find_packages(),
    long_description=open(join(dirname(__file__), 'README.md')).read(),
    long_description_content_type="text/markdown",
    install_requires=requirements,
    author="Nooblord",
    entry_points={
        'console_scripts':
            ['ydsync=yandexdirsync.__main__:main']
    }
)
